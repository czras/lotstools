#!/bin/bash

HELP_TEXT="Usage:
 convert_raidlist2text.sh <game> <name of HTML source> <timestamp string>"

GAME=$1
SOURCENAME=$2
TIMESTAMP=$3

case $GAME in
  lots)
    LINEBREAK="<\/tr>"
    GAMENAME="legacy-of-a-thousand-suns"
    LINESTART="(<tbody>\)*<[^>]*><[^>]*>[-0-9: ]*<[^>]*><[^>]*>"
    LINEEND="[^>]*<\/a>\(<[^>]*>\)*"
    ;;

  dotd)
    LINEBREAK="<\/div>"
    GAMENAME="dawn-of-the-dragons"
    LINESTART="(<div [^>]*>\)*<span [^>]*>([0-9:]*)<\/span> <span [^>]*>\[[a-zA-Z ]*\]<\/span>: "
    LINEEND="<span [^>]*>[a-zA-Z -]*<\/span> <span [^>]*>\[[A-Z]*\]<\/span>[^>]*<\/a> \([^>]*>\)*"
    ;;

  *)
    echo $HELP_TEXT
    return 0
    ;;

esac

LINKSTART="http:\/\/www\.kongregate\.com\/games\/5thPlanetGames\/"$GAMENAME"?kv_action_type=raidhelp&"

# add newlines after each link
sed 's/'"$LINEBREAK"'/'"$LINEBREAK"'\
/g' $SOURCENAME.html -i


# game specific text preprocessing
case $GAME in
  lots)
    # remove html body
    sed '1,14 d' <$SOURCENAME.html >$SOURCENAME.txt
    ;;
  dotd)
    # correct ampersands
    sed 's/\&amp;/\&/g' <$SOURCENAME.html >$SOURCENAME.txt
    ;;

esac

if [ -n "$TIMESTAMP" ]; then
# delete all lines older than timestamp
    case $GAME in
	  lots)
    	sed '/'"$TIMESTAMP"'/,$ d' $SOURCENAME.txt -i
    	;;
	  dotd)    
    	sed '1,/'"$TIMESTAMP"'/ d' $SOURCENAME.txt -i
   		;;
	esac
fi
	
# erase everything before link
sed 's/\'"$LINESTART"'//g' $SOURCENAME.txt -i
# get the link and erase everything after
sed 's/<a href="\('"$LINKSTART"'[a-zA-Z0-9&=_]*\)"[^>]*>'"$LINEEND"'/\1/g' $SOURCENAME.txt -i

# filter for different raids and write them to separate text files
case $GAME in
  lots)
    RAIDS1="void telemachus carnifex carnus rautha tulk china assasin"
    RAIDS2="colonel vespasia generalrahn"
    RAIDS3="centurian_sentinel mermara besalaad_warmaster reaver xarpa dule_warmaster"

#    for RAIDNAME in [ RAIDS1 RAIDS 2 RAIDS 3]; do
#      grep $RAIDNAME $SOURCENAME.txt >$SOURCENAME-$RAIDNAME.txt

    grep 'carnifex\|carnus\|rautha\|colonel\|void\|telemachus' $SOURCENAME.txt >$SOURCENAME-allsmall.txt
    grep 'natasha\|tulk\|china\|assasin\|vespasia\|cruiser\|generalrahn' $SOURCENAME.txt >$SOURCENAME-allmed.txt
    grep 'sun_xi'  $SOURCENAME.txt >$SOURCENAME-sunxi.txt
    grep 'besalaad_warmaster' $SOURCENAME.txt >$SOURCENAME-warmaster.txt
    grep 'centurian_sentinel' $SOURCENAME.txt >$SOURCENAME-sentinel.txt
    grep 'mermara' $SOURCENAME.txt >$SOURCENAME-mermara.txt
    grep 'nemo' $SOURCENAME.txt >$SOURCENAME-nemo.txt
    grep 'reaver\|xarpa\|sigurd\|dule_warmaster' $SOURCENAME.txt >$SOURCENAME-z09.txt
    grep 'the_hat\|crush_colossa\|kalaxian_cult_mistress\|bachanghenfil' $SOURCENAME.txt >$SOURCENAME-z10.txt
    ;;

  dotd)
    RAIDS1="scorp rhino ogre alice"
    RAIDS2="drag bellarius batman lurker"
    RAIDS3="3dawg"
	
	  grep 'rhino\|scorp\|maraak\|ogre' $SOURCENAME.txt >$SOURCENAME-allsmall.txt
	  grep 'kv_raid_boss=drag&' $SOURCENAME.txt >$SOURCENAME-erebus.txt
	  grep 'lurker' $SOURCENAME.txt >$SOURCENAME-horror.txt
	  grep 'tainted' $SOURCENAME.txt >$SOURCENAME-tainted.txt
	  grep 'batman' $SOURCENAME.txt >$SOURCENAME-gravlok.txt
	  grep 'bogstench' $SOURCENAME.txt >$SOURCENAME-bogstench.txt
	  grep 'ulfrik' $SOURCENAME.txt >$SOURCENAME-ulfrik.txt
	  grep 'guilbert' $SOURCENAME.txt >$SOURCENAME-guilbert.txt
    grep '3dawg' $SOURCENAME.txt >$SOURCENAME-kerberos.txt
    grep 'bellarius' $SOURCENAME.txt >$SOURCENAME-bellarius.txt
  	grep 'erakka_sak' $SOURCENAME.txt >$SOURCENAME-erakka-sak.txt
	  grep 'wexxa' $SOURCENAME.txt >$SOURCENAME-wexxa.txt
    ;;

esac

